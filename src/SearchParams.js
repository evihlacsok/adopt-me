import React, { useState, useEffect, useContext } from 'react';
import pet, { ANIMALS } from '@frontendmasters/pet';
import Results from './Results';
import useDropdown from './useDropdown';
import { async } from 'q';
import ThemeContext from './ThemeContext';

const SearchParams = () => {
    const [location, setLocation] = useState("Seattle, WA");
    const [breeds, setBreeds] = useState([]);
    const [animal, AnimalDropdown] = useDropdown("Animal", "dog", ANIMALS);
    const [breed, BreedDropdown, setBreed] = useDropdown("Breed", "", breeds);
    const [pets, setPets] = useState([]);
    const [theme, setTheme] = useContext(ThemeContext);

    async function requestPets() {
        const {animals} = await pet.animals({
            location,
            breed,
            type: animal
        });

        setPets(animals || []);
    }

    useEffect(() => {
        setBreeds([]);
        setBreed("");

        pet.breeds(animal).then(({breeds: apiBreeds}) => {
            const breedStrings = apiBreeds.map(({name}) => name);
            setBreeds(breedStrings);
        }, console.error);
    }, [animal, setBreed, setBreeds]);

    return (
        <div className="search-params">
            <form onSubmit={(e) => {
                e.preventDefault();
                requestPets();
            }}>
                <h1>{location}</h1>
                <label htmlFor="location">
                    Location
                    <input
                        id="location"
                        value={location}
                        onChange={event => setLocation(event.target.value)}
                        placeholder="Location"/>
                </label>

                <AnimalDropdown/>
                <BreedDropdown/>

                <label htmlFor="theme">
                    Theme
                    <select
                        value={theme.buttonColor}
                        onChange={e => setTheme({buttonColor: e.target.value})}
                        onBlur={e => setTheme({buttonColor: e.target.value})} >
                            <option value="peru">Peru</option>
                            <option value="darkblue">Darkblue</option>
                            <option value="yellow">Yellow</option>
                            <option value="pink">Pink</option>
                    </select>
                </label>

                {/* <label htmlFor="animal">
                    Animal
                    <select
                        id="animal"
                        value={animal}
                        onChange={event => setAnimal(event.target.value)}
                        onBlur={event => setAnimal(event.target.value)}>
                            <option>All</option>
                            {ANIMALS.map(animal =>
                                <option key={animal} value={animal}>{animal}</option>)}
                    </select>
                </label>

                <label htmlFor="breed">
                    breed
                    <select
                        id="breed"
                        value={breed}
                        onChange={event => setBreed(event.target.value)}
                        onBlur={event => setBreed(event.target.value)}
                        disabled={!breed.length}>
                            <option>All</option>
                            {breeds.map(breedString =>
                                <option key={breedString} value={breedString}>{breedString}</option>)}
                    </select>
                </label> */}

                <button style={{backgroundColor: theme.buttonColor}}>Submit</button>
            </form>
            <Results pets={pets} />
        </div>
    )
}

export default SearchParams;